tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start:
 <name;separator=\" \n\"> ";

decl  :
        ^(VAR id=ID) {globals.newSymbol($id.text);} -> dek(n={$id.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$id);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) {
                                      if(Integer.parseInt($e2.text) == 0){
                                        throw new ArithmeticException();
                                      }
                                   } -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST id=ID   e2=expr) {
                                      if(!globals.hasSymbol($id.text)){
                                        throw new RuntimeException();
                                      }
                                      globals.setSymbol($id.text,Integer.parseInt($e2.text));
                                   } -> podstaw(name={$id.text}, val={$e2.st})
        | ID {
              if(!globals.hasSymbol($ID.text)){
                throw new RuntimeException();
              }
             } -> odczyt(name={$ID.text})
        | INT  {numer++;} -> int(i={$INT.text},j={numer.toString()})
    ;
    