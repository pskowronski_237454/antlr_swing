tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

// Program może zawierać instrukcje drukujące, delaracje oraz pozostałe wyrażenia (expr)
prog    : ( print | decl | e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;

// Reguła gramatyczna dla deklaracji zmiennych globalnych oraz ich ewentualnej inicjalizacji
decl  : ^(VAR id=ID) {globals.newSymbol($id.text);}
      | ^(VAR id=ID val=expr) {globals.newSymbol($id.text); globals.setSymbol($id.text, $val.out);}
      ;

// Reguła gramatyczna dla instrukcji Print
print
      : ^(PRINT expr) {System.out.println($expr.out.toString());}
      ;
    
expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {
                                      if($e2.out==0){
                                        throw new ArithmeticException();
                                      }else{
                                        $out = $e1.out / $e2.out;
                                      }  
                                   }
        | ^(SHL val=expr pos=expr) {$out = $val.out << $pos.out;} // Przesunięcie bitowe w lewo
        | ^(SHR val=expr pos=expr) {$out = $val.out >> $pos.out;} // Przesunięcie bitowe w prawo
        | ^(PODST i1=ID e2=expr)  { globals.setSymbol($i1.text, $e2.out); $out = $e2.out;} // Podstawienie wartości do zmiennej
        | ID  {$out = globals.getSymbol($ID.text);}    // Zwrócenie wartości zmiennej globalnej
        | INT {$out = getInt($INT.text);}
        ;

