grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

// Dodanie alternatyw: intrukcja drukująca oraz deklaracja zmiennej globalnej wraz inicjalizacją
stat
    : expr NL -> expr
    | PRINT expr NL -> ^(PRINT expr)
    | VAR ID NL -> ^(VAR ID)
    | VAR ID PODST expr NL -> ^(VAR ID expr)
    | ID PODST expr NL -> ^(PODST ID expr)
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;

// Dodanie alternatyw dla przesunięć bitowych
multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | SHL^ atom
      | SHR^ atom
      )*
    ;


atom
    : INT
    | ID
    | LP! expr RP!
    ;

// Leksem dla intrukcji drukującej
PRINT : 'Print' ;

VAR :'var';

// Leksem dla przesunięcia bitowego w lewo
SHL : '<<' ;

// Leksem dla przesunięcia bitowego w prawo
SHR : '>>' ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;




LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
